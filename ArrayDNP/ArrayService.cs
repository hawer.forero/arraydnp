﻿using System;
namespace ArrayDNP
{
    public class ArrayService
    {
        /// <summary>
        /// Calcula el número consecutivo de menor valor, que sea mayor a cero y que no exista en dicho arreglo
        /// </summary>
        /// <param name="array">Array de enteros</param>
        /// <returns>Número consecutivo</returns>
        public int Calcular(int[] array) {

            //Inicializadores
            var consecutivo = 1;

            //Ordenar array
            Array.Sort(array);

            for (int i=0; i< array.Length; i++)
            {
                //Obtener siguiente valor
                consecutivo = array[i] + 1;

                //Validar si es el ultimo elemento
                if (array[i + 1] >= array.Length)
                {
                    break;
                }

                //Validar el ultimo consecutivo
                if(consecutivo == array[i + 1] || array[i] == array[i + 1])
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            //Debe ser mayor a 0
            if(consecutivo <= 0)
            {
                consecutivo = 1;
            }

            //Salida
            return consecutivo;
        }
    }
}
