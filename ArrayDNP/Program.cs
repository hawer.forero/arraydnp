﻿using System;

namespace ArrayDNP
{
    class Program
    {
        static void Main(string[] args)
        {
            var arrayService = new ArrayService();
            int[] example = new int[8] { 7, 9, 5, 3, 2, 1, 3, 1 };
            Console.WriteLine(arrayService.Calcular(example));

            int[] example2 = new int[3] { 1, 2, 4 };
            Console.WriteLine(arrayService.Calcular(example2));

            int[] example3 = new int[3] {-8,-4,-7};
            Console.WriteLine(arrayService.Calcular(example3));
          
        }
    }
}
