﻿using NUnit.Framework;
using ArrayDNP;
namespace TestArray
{
    public class ArrayServiceTest
    {
        private ArrayService arrayService;

        [SetUp]
        public void Setup()
        {
            arrayService = new ArrayService();

        }
        [Test]
        public void CalcularTest1()
        {
            int[] data = new int[8] { 7, 9, 5, 3, 2, 1, 3, 1 };
            arrayService.Calcular(data);

            Assert.AreEqual(arrayService.Calcular(data), 4);
        }

        [Test]
        public void CalcularTest2()
        {
            int[] data = new int[3] { 1, 2, 4 };
            arrayService.Calcular(data);

            Assert.AreEqual(arrayService.Calcular(data), 3);
        }

        [Test]
        public void CalcularTest3()
        {
            int[] data = new int[3] { -8, -4, -7 };
            arrayService.Calcular(data);

            Assert.AreEqual(arrayService.Calcular(data), 1);
        }
    }
}
